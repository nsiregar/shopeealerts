class ShopOfferV2:
    def __init__(self, shopId, shopName, commissionRate):
        self.shop_id = shopId
        self.commission_rate = commissionRate
        self.shop_name = shopName

    def product_payload(self, limit=20):
        query_payload = {
            "query": f"""{{
                productOfferV2(listType: 5, matchId: {self.shop_id}, limit: {limit}) {{
                    nodes {{
                        itemId,
                        imageUrl,
                        price,
                        productName,
                        productLink,
                        offerLink,
                        commissionRate,
                    }}
                }}
            }}"""
        }
        return query_payload


class ShopOfferV2Query:
    @classmethod
    def payload(cls, keyword="", sort_type=1, limit=20):
        query_payload = {
            "query": f"""{{
              shopOfferV2(
                 keyword:"{keyword}",
                 sortType: {sort_type},
                 limit: {limit}){{
                    nodes {{
                      commissionRate,
                      shopId,
                      shopName,
                    }}
              }}
           }}"""
        }
        return query_payload


class ProductOfferV2:
    def __init__(
        self,
        item_id,
        image_url,
        offer_link,
        product_name,
        price,
        product_link,
        commission_rate,
        schedule_time,
    ):
        self.item_id = item_id
        self.image_url = image_url
        self.offer_link = offer_link
        self.product_name = product_name
        self.price = price
        self.product_link = product_link
        self.commission_rate = commission_rate
        self.schedule_time = schedule_time

    def __str__(self):
        return f"Item {self.item_id} scheduled at {self.schedule_time}"
