import os
import time

import requests

import tweepy
from tweepy import Client
from config import config

client = Client(
    bearer_token=config.TWITTER_BEARER_TOKEN,
    consumer_key=config.TWITTER_API_KEY,
    consumer_secret=config.TWITTER_API_SECRET,
    access_token=config.TWITTER_ACCESS_TOKEN,
    access_token_secret=config.TWITTER_ACCESS_TOKEN_SECRET
)
auth = tweepy.OAuthHandler(config.TWITTER_API_KEY, config.TWITTER_API_SECRET)
auth.set_access_token(config.TWITTER_ACCESS_TOKEN, config.TWITTER_ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)


def get_trends():
    jakarta_woeid = 1047378
    trends = api.get_place_trends(jakarta_woeid)
    trends = trends[0]["trends"]
    hashtags = [trend["name"] for trend in trends]
    return hashtags[:5]


def create_thread(shop_offer):
    shop_name = shop_offer['shop_name']
    thread_title = [f"Tawaran Terbaru dari Toko {shop_name} di #Shopee\n"]
    trends = get_trends()
    thread_title.extend(trends)
    thread_caption = '\n'.join(thread_title)

    initial_tweet = client.create_tweet(text=thread_caption)
    tweet_id = initial_tweet.data.get("id")

    for product in shop_offer['products']:
        try:
            resp = reply_tweet(product, tweet_id)
            tweet_id = resp.data.get("id")
            time.sleep(15)
        except Exception as exc:
            print(exc)


def reply_tweet(product, tweet_id=None):
    title = f"{product.product_name}\n\n"
    caption = f"{product.offer_link}\n"

    if len(caption) + len(title) < 280:
        caption = title + caption

    filename = f"temp-{product.item_id}.jpg"
    try:
        resp = requests.get(product.image_url, stream=True)
        if resp.status_code == 200:
            with open(filename, 'wb') as image:
                for chunk in resp:
                    image.write(chunk)
            media = api.media_upload(filename)
            tweet_resp = client.create_tweet(
                text=caption,
                media_ids=[media.media_id],
                in_reply_to_tweet_id=tweet_id,
            )
            os.remove(filename)
            return tweet_resp
    except Exception as exc:
        print(exc)


def send_tweet(product):
    trends = get_trends()
    title_text = [product.product_name, '\n']
    caption_text = [f"{product.offer_link}\n"]
    caption_text.extend(trends)
    caption = '\n'.join(caption_text)
    title = '\n'.join(title_text)

    if len(caption) + len(title) < 280:
        caption = title + caption

    filename = f"temp-{product.item_id}.jpg"
    try:
        resp = requests.get(product.image_url, stream=True)
        if resp.status_code == 200:
            with open(filename, 'wb') as image:
                for chunk in resp:
                    image.write(chunk)
            media = api.media_upload(filename)
            client.create_tweet(
                text=caption,
                media_ids=[media.media_id],
            )
            os.remove(filename)
    except Exception as exc:
        print(exc)
