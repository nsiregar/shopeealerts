from telegram import Bot
from config import config

bot = Bot(config.TELEGRAM_BOT_TOKEN)


def send_photo(product):
    caption_text = [
        f"{product.product_name}\n",
        f"{product.offer_link}\n",
        "Follow us on @shopeesalealert for the latest updates",
    ]
    caption = '\n'.join(caption_text)
    bot.sendPhoto(config.TELEGRAM_CHANNEL_ID, photo=product.image_url, caption=caption)
