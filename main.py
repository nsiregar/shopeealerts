import asyncio
import atexit
import datetime

from redis import Redis
from rq import Queue

from client.affiliate import ShopeeAffiliate
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from core.message.telegram import send_photo
from core.message.twitter import create_thread
from config import config

q = Queue(connection=Redis('redis', 6379), default_timeout=600)


class AffiliateApp:
    def __init__(self):
        self.client = ShopeeAffiliate(config.APP_ID, config.APP_SECRET)

    async def schedule_post(self):
        products = await self.client.get_product_offer()
        for product in products:
            q.enqueue_at(product.schedule_time, send_photo, product)

        shop_product_offers = await self.client.get_shop_product_offer()
        for key, value in shop_product_offers.items():
            q.enqueue_at(value['schedule_time'], create_thread, value)


if __name__ == "__main__":
    scheduler = AsyncIOScheduler(timezone="UTC")
    app = AffiliateApp()
    scheduler.add_job(app.schedule_post, "interval", hours=24, next_run_time=datetime.datetime.now())
    scheduler.start()
    atexit.register(scheduler.shutdown)

    try:
        asyncio.get_event_loop().run_forever()
    except (KeyboardInterrupt, SystemExit):
        pass
