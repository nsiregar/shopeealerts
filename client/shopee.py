import hashlib
import datetime
import calendar
import json
import aiohttp

from dataclasses import dataclass

SHOPEE_OPEN_API_BASE_URL = "https://open-api.affiliate.shopee.co.id"


@dataclass
class ShopeeApp:
    id: str
    secret: str


class ShopeeRequest:
    def __init__(self, app: ShopeeApp, payload: dict):
        self.app = app
        self.payload = payload
        self.current_time = datetime.datetime.utcnow()
        self.timestamp = calendar.timegm(self.current_time.utctimetuple())

    @property
    def signature(self) -> str:
        payload = json.dumps(self.payload)
        signature = f"{self.app.id}{self.timestamp}{payload}{self.app.secret}"
        hashing = hashlib.sha256(signature.encode("utf-8"))
        return hashing.hexdigest()


class ShopeeClient:
    def __init__(self, app_id: str, secret: str):
        self.app = ShopeeApp(app_id, secret)

    async def post(self, payload: dict):
        req = ShopeeRequest(self.app, payload)
        headers = {
            "Authorization": f"SHA256 Credential={self.app.id}, Signature={req.signature}, Timestamp={req.timestamp}",
            "Content-Type": "application/json",
        }
        aio_session = aiohttp.ClientSession(base_url=SHOPEE_OPEN_API_BASE_URL)
        async with aio_session as session:
            resp = await session.post("/graphql", headers=headers, json=req.payload)
        return resp
