from .shopee import ShopeeClient
from core.ops.shortlink import ShortLink
from core.ops.shop_offer import ShopOfferV2Query, ShopOfferV2, ProductOfferV2
import datetime


class ShopeeAffiliate:
    def __init__(self, app_id, secret):
        self.client = ShopeeClient(app_id=app_id, secret=secret)

    async def generate_shortlink(self, url, sub_ids=None):
        shopee_link = ShortLink(origin_url=url, sub_ids=sub_ids)
        resp = await self.client.post(payload=shopee_link.payload)
        data = await resp.json()
        shortlink = data["data"]["generateShortLink"]["shortLink"]
        return shortlink

    async def get_shop_offer(self):
        payload = ShopOfferV2Query.payload()
        resp = await self.client.post(payload=payload)
        data = await resp.json()
        shop_nodes = data["data"]["shopOfferV2"]["nodes"]
        print(shop_nodes)
        shop_nodes = [
            ShopOfferV2(shopId=item['shopId'], shopName=item['shopName'], commissionRate=item['commissionRate']) for
            item in shop_nodes]
        return shop_nodes

    async def get_product_offer(self):
        shop_offers = await self.get_shop_offer()
        current_time = datetime.datetime.utcnow()
        products = []

        for offer in shop_offers:
            schedule_time = current_time + datetime.timedelta(seconds=30)
            resp = await self.client.post(payload=offer.product_payload())
            data = await resp.json()
            product_offers = data["data"]["productOfferV2"]["nodes"]
            for product in product_offers:
                product_offer = ProductOfferV2(
                    item_id=product["itemId"],
                    image_url=product["imageUrl"],
                    offer_link=product["offerLink"],
                    product_name=product["productName"],
                    price=product["price"],
                    product_link=product["productLink"],
                    commission_rate=product["commissionRate"],
                    schedule_time=schedule_time,
                )
                products.append(product_offer)

            current_time += datetime.timedelta(hours=1)

        return products

    async def get_shop_product_offer(self):
        current_time = datetime.datetime.utcnow()
        shop_offers = await self.get_shop_offer()
        shop_product_offers = {}

        for offer in shop_offers:
            shop_product_offers[offer.shop_id] = {}
            shop_product_offers[offer.shop_id]['shop_name'] = offer.shop_name
            shop_product_offers[offer.shop_id]['products'] = []
            shop_product_offers[offer.shop_id]['schedule_time'] = current_time

            resp = await self.client.post(payload=offer.product_payload())
            data = await resp.json()
            product_offers = data["data"]["productOfferV2"]["nodes"]
            for product in product_offers:
                product_offer = ProductOfferV2(
                    item_id=product["itemId"],
                    image_url=product["imageUrl"],
                    offer_link=product["offerLink"],
                    product_name=product["productName"],
                    price=product["price"],
                    product_link=product["productLink"],
                    commission_rate=product["commissionRate"],
                    schedule_time=None,
                )
                shop_product_offers[offer.shop_id]['products'].append(product_offer)

            current_time += datetime.timedelta(hours=1)

        return shop_product_offers
